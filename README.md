A quick tool to transfer savefiles without the knowledge about these files and what to do.

Make sure you made a savegame in both copies of HLD. Copy the contents from one file and the contents from the other file in this tool and click the transfer button to combine the files into your new save game.